from time import sleep

from cv2 import TM_CCOEFF_NORMED, imread, matchTemplate
from numpy import where
from PIL import ImageGrab
from pyautogui import click, moveTo

if __name__ == "__main__":
    print("Running...")
    while True:
        # Screenshot
        screenshot = ImageGrab.grab()
        screenshot.save("temp.png")

        # Load images
        main_image = imread("temp.png")
        template_image = imread("goldburger.png")

        # Find the burger
        result = matchTemplate(main_image, template_image, TM_CCOEFF_NORMED)
        threshold = 0.6
        locations = where(result >= threshold)

        # Fetch coordinates
        locations = list(zip(*locations[::-1]))

        # If we find a burger
        if len(locations):
            x, y = locations[0][0] + 20, locations[0][1] + 10
            print(f"Find a golden burger at {x}x{y}")

            # Move and click
            moveTo(x, y)
            click()

        # Wait a second every loop
        sleep(1)
