# **G**olden **B**urger **C**licker

- Run `run.bat`

Tested with [AlphaClicker](https://github.com/robiot/AlphaClicker) setup with:

- A random click interval between **0.3** and **0.4** seconds
- Location forced on the **location of the burger**
