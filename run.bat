@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Variables
SET "pipR=pip install -r .\requirements.txt"
SET "py=python.exe"

:: Check python installation
!py! --version >NUL

:: Clean potential error or python version
CLS

IF ERRORLEVEL 1 (
    ECHO Installation of Python not found, installation...

    :: Install Python 3.11 from MS Store
    ECHO Y | winget install -he 9NRWMJP3717K

    ECHO Download and installation of dependencies...

    :: Location of Python
    SET "py=%LOCALAPPDATA%\Microsoft\WindowsApps\!py!"

    :: Update pip
    !py! -m pip install --upgrade pip

    :: Install dependencies
    !py! -m !pipR!
) ELSE (
    ECHO Checking dependencies...

    !py! -m !pipR! >NUL

    ECHO Dependencies checked!
)

:: Run app
!py! .\main.py

EXIT /B
